import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { KatalogComponent } from '../katalog/katalog.component';
import { BerandaComponent } from '../beranda/beranda.component';
import { PembayaranComponent } from '../pembayaran/pembayaran.component';
import { KeranjangComponent } from '../keranjang/keranjang.component';


const routes: Routes = [
  {
    children:[
      {
        path:'beranda',
        component:BerandaComponent
      },
      {
        path:'keranjang',
        component:KeranjangComponent
      },
      {
        path:'pembayaran',
        component:PembayaranComponent
      },
      {
        path:'catalog',
        component:KatalogComponent
      },
      {
        path:'',
        redirectTo:'/admin/beranda',
        pathMatch:'full'
      }
    ]
  },
  
]


@NgModule({
  declarations: [ BerandaComponent, KatalogComponent, KeranjangComponent, PembayaranComponent],
  entryComponents:[
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class AdminModule { }
