import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BerandaComponent } from './beranda/beranda.component';
import { KatalogComponent } from './katalog/katalog.component';
import { KeranjangComponent } from './keranjang/keranjang.component';
import { PembayaranComponent } from './pembayaran/pembayaran.component';

@NgModule({
  declarations: [
    AppComponent,
    BerandaComponent,
    KatalogComponent,
    KeranjangComponent,
    PembayaranComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
