import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BerandaComponent } from './beranda/beranda.component';
import { KatalogComponent } from './katalog/katalog.component';
import { KeranjangComponent } from './keranjang/keranjang.component';
import { PembayaranComponent } from './pembayaran/pembayaran.component';

const routes: Routes = [
  {
    path:'beranda',
    component:BerandaComponent
  },
  {
    path:'katalog',
    component:KatalogComponent
  },
  {
    path:'keranjang',
    component:KeranjangComponent
  },
  {
    path:'pembayaran',
    component:PembayaranComponent
  },
  {
    path:'',
    redirectTo:'/beranda',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
